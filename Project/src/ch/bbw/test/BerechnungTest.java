package ch.bbw.test;

import ch.bbw.java.Berechnung;
import org.junit.Test;

import static org.junit.Assert.*;

public class BerechnungTest {

    @Test
    public void BerechnungenTest() {
        Berechnung berechnung = new Berechnung(100, 10, 10, 10, 10, "test");

        assertEquals(8, (int) berechnung.getRate());
        assertEquals(80, (int) berechnung.getAmortisation());
        assertEquals(4, (int) berechnung.getZinskosten());
    }
}