package ch.bbw.java;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    ComboBox<String> laufzeit;

    @FXML
    TextField netto, anzahlung, rest;

    @FXML
    Spinner<Double> zins;

    @FXML
    Button berechnen;

    @FXML
    TextField rate, amortisation, zinsausgabe, name;

    @FXML
    Button save, load;

    @FXML
    ListView<Berechnung> laufzeitlist;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        laufzeitlist.setItems(Main.data);

        ComboboxSetup();

        berechnenSetup();

        saveSetup();

        loadSetup();
    }

    private void ComboboxSetup() {
        ObservableList<String> list = FXCollections.observableArrayList();
        list.add("3  Monate");
        list.add("6  Monate");
        list.add("12 Monate");
        list.add("24 Monate");
        list.add("36 Monate");
        list.add("48 Monate");
        laufzeit.setItems(list);
    }

    private void berechnenSetup() {
        try {
            berechnen.setOnAction(e -> {
                Berechnung berechnung = new Berechnung(Integer.parseInt(netto.getText()), Integer.parseInt(anzahlung.getText()), zins.getValue(), Integer.parseInt(laufzeit.getSelectionModel().getSelectedItem().substring(0, 2).replace(" ", "")), Integer.parseInt(rest.getText()));
                rate.setText((String.valueOf(berechnung.getRate())));
                amortisation.setText((String.valueOf(berechnung.getAmortisation())));
                zinsausgabe.setText((String.valueOf(berechnung.getZinskosten())));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void saveSetup() {
        try {
            save.setOnAction(e -> {
                Main.data.add(new Berechnung(Integer.parseInt(netto.getText()), Integer.parseInt(anzahlung.getText()), zins.getValue(), Integer.parseInt(laufzeit.getSelectionModel().getSelectedItem().substring(0, 2).replace(" ", "")), Integer.parseInt(rest.getText()), name.getText()));
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadSetup() {
        load.setOnAction(e -> {
            Berechnung berechnung = laufzeitlist.getSelectionModel().getSelectedItem();
            netto.setText(String.valueOf(berechnung.getNetto()));
            anzahlung.setText(String.valueOf(berechnung.getAnzahlung()));

            zins.decrement(2000);
            zins.increment((int) (berechnung.getZins() * 10));

            laufzeit.setPromptText(berechnung.getMoate() + " Monate");
            rest.setText(String.valueOf(berechnung.getRestwert()));
            rate.setText(String.valueOf(berechnung.getRate()));
            amortisation.setText(String.valueOf(berechnung.getAmortisation()));
            zinsausgabe.setText(String.valueOf(berechnung.getZinskosten()));
        });
    }


}
