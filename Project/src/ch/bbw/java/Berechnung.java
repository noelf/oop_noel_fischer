package ch.bbw.java;

public class Berechnung {

    public Berechnung() {
        this(0, 0, 0, 0, 1);
    }

    public Berechnung(int netto, int anzahlung, double zins, int monate, int restwert) {

        this.netto = netto;
        this.anzahlung = anzahlung;
        this.zins = zins;
        this.moate = monate;
        this.restwert = restwert;

        double jahre = monate / 12.0;
        netto = netto - anzahlung;

        zinskosten = ((double) (netto + restwert) * zins * jahre) / (2 * 100);
        amortisation = netto - restwert;
        rate = (zinskosten + amortisation) / monate;
    }

    public Berechnung(int netto, int anzahlung, double zins, int monate, int restwert, String name) {
        this(netto, anzahlung, zins, monate, restwert);
        this.name = name;
    }

    private int netto;
    private int anzahlung;
    private double zins;
    private int moate;
    private int restwert;

    private double zinskosten;
    private double amortisation;
    private double rate;

    public int getNetto() {
        return netto;
    }

    public void setNetto(int netto) {
        this.netto = netto;
    }

    public int getAnzahlung() {
        return anzahlung;
    }

    public void setAnzahlung(int anzahlung) {
        this.anzahlung = anzahlung;
    }

    public double getZins() {
        return zins;
    }

    public void setZins(double zins) {
        this.zins = zins;
    }

    public int getMoate() {
        return moate;
    }

    public void setMoate(int moate) {
        this.moate = moate;
    }

    public int getRestwert() {
        return restwert;
    }

    public void setRestwert(int restwert) {
        this.restwert = restwert;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public double getZinskosten() {
        return zinskosten;
    }

    public void setZinskosten(double zinskosten) {
        this.zinskosten = zinskosten;
    }

    public double getAmortisation() {
        return amortisation;
    }

    public void setAmortisation(double amortisation) {
        this.amortisation = amortisation;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    @Override
    public String toString() {
        return name;
    }
}
